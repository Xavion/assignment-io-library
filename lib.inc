section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall
    ret

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
.loop:
    cmp byte[rdi + rax], 0
    je .end
    inc rax
    jmp .loop
.end:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	call string_length
   	mov rdx, rax
   	mov rax, 1
   	mov rsi, rdi
   	mov rdi, 1
   	syscall
   	ret


; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA

; Принимает код символа и выводит его в stdout
print_char:
	push rdi
    mov rax, 1
    mov rdi, 1
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rdi
    ret



; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov r10, 10
    mov rax, rdi
    mov rdi, rsp
    dec rdi
    push 0
    sub rsp, 16
.loop:
    xor rdx, rdx
    div r10
    add rdx, '0'
    dec rdi
    mov [rdi], dl
    cmp rax, 0
    jne .loop
.end:
    call print_string
    add rsp, 24
    ret


; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0
    jl .negative
.positive:
    jmp print_uint
.negative:
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
    jmp print_uint


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rcx, rcx
    xor r9, r9
    xor r10, r10
.loop:
    mov r9b, byte [rdi + rcx]
    mov r10b, byte [rsi + rcx]
    cmp r9b, r10b
    jne .not_equals
    cmp r9b, 0
    je .equals
    inc rcx
    jmp .loop
.equals:                 
    mov rax, 1
    ret
.not_equals:             
    mov rax, 0
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    mov rax, 0
    push rax
    mov rdi, 0
    mov rdx, 1
    mov rsi, rsp
    syscall
    pop rax
    ret


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push rbx
    mov r9, rdi
    mov r10, rsi
    xor rbx, rbx
.loop:
    call read_char
    cmp al, ' '
    je .loop
    cmp al, '	'
    je .loop
    cmp al, '\n'
    je .loop
    cmp al, 0
    je .end
    jmp .word_read
.word_read:
    mov byte[r9 + rbx], al
    inc rbx
    call read_char
    cmp al, ' '
    je .end
    cmp al, '	'
    je .end
    cmp al, '\n'
    je .end
    cmp al, 0
    je .end
    cmp r10, rbx
    ja .word_read
    jmp .overflow
.overflow:
    xor rax, rax
    pop rbx
    ret
.end:
    mov rax, r9
    mov rdx, rbx
    mov byte[r9 + rbx], 0
    pop rbx
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rcx, rcx
    xor rax, rax
    xor r8, r8
    mov r10, 10
.num_interval:
    cmp byte[rdi+rcx], '0'
    jb .not_currect
    cmp byte[rdi+rcx], '9'
    ja .not_currect
    mul r10
    mov r8b, byte[rdi+rcx]
    sub r8b, '0'
    add rax, r8
    inc rcx
    jmp .num_interval
.not_currect:
    mov rdx, rcx
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], '-'
    jne parse_uint
    inc rdi
    call parse_uint
    neg rax
    inc rdx
    ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
push rbx
    xor rax, rax
.loop:
    cmp rax, rdx
    je .err
    mov bl, byte [rdi + rax]
    mov byte [rsi + rax], bl
    cmp byte [rsi + rax], 0
    je .end
    inc rax
    jmp .loop
.err:
    xor rax, rax
.end:
    pop rbx
    ret
